ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .enablePlugins(ScalaJSBundlerPlugin)
  .settings(
    name := "react-admin-tutorial-scala",
    idePackagePrefix := Some("tutorial"),
    scalaJSUseMainModuleInitializer := true,
    scalaJSLinkerConfig := scalaJSLinkerConfig.value.withSourceMap(false),
    webpackBundlingMode := BundlingMode.LibraryOnly(),
    libraryDependencies ++= Seq(
      "com.github.japgolly.scalajs-react" %%% "core" % "2.1.1",
      "io.github.nafg.scalajs-facades" %%% "simplefacade" % "0.16.0"
    ),
    Compile / npmDependencies ++= Seq(
      "@mui/icons-material" -> "^5.0.1",
      "@mui/material" -> "^5.0.2",
      "ra-data-json-server" -> "^4.0.0",
      "react" -> "^17.0.0",
      "react-admin" -> "^4.0.0",
      "react-dom" -> "^17.0.0",
      "react-scripts" -> "^5.0.0"
    )
  )
