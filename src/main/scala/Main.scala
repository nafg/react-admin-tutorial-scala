package tutorial

import scala.scalajs.js
import scala.scalajs.js.Dynamic.{literal => lit}

import org.scalajs.dom.document
import japgolly.scalajs.react.ScalaFnComponent
import japgolly.scalajs.react.vdom.html_<^._

import facades.mui.{LaunchIcon, Link}
import facades.reactadmin.ReactAdmin.useRecordContext
import facades.reactadmin._

object MyUrlField {
  val component =
    ScalaFnComponent
      .withHooks[String]
      .unchecked(useRecordContext())
      .render { (source, record) =>
        val data = record.get(source).map(_.asInstanceOf[String]).orNull
        Link(_.href := data, _.sx := lit(textDecoration = "none"))(
          data,
          LaunchIcon(
            _.sx := lit(width = "0.5em", height = "0.5em", paddingLeft = 2)
          )
        )
      }
}

object UserList {
  val component = ScalaFnComponent[Unit] { _ =>
    List()(
      Datagrid(_.rowClick := "edit")(
        TextField(_.source := "id"),
        TextField(_.source := "name"),
        EmailField(_.source := "email"),
        TextField(_.source := "phone"),
        MyUrlField.component("website"),
        TextField(_.source := "company.name")
      )
    )
  }

  def asElementType = component.toJsComponent.raw
}

object PostList {
  val component = ScalaFnComponent[Unit] { _ =>
    List()(
      Datagrid()(
        TextField(_.source := "id"),
        ReferenceField(_.source := "userId", _.reference := "users")(
          TextField(_.source := "name")
        ),
        TextField(_.source := "title"),
        EditButton()
      )
    )
  }

  def asElementType = component.toJsComponent.raw
}

object PostEdit {
  val component = ScalaFnComponent[Unit] { _ =>
    Edit()(
      SimpleForm()(
        TextInput(_.disabled, _.source := "id"),
        ReferenceInput(_.source := "userId", _.reference := "users")(
          SelectInput(_.optionText := "name")
        ),
        TextInput(_.source := "title"),
        TextInput(_.source := "body")
      )
    )
  }

  def asElementType = component.toJsComponent.raw
}

object PostCreate {
  val component = ScalaFnComponent[js.Object] { props =>
    Create(props.asInstanceOf[js.Dictionary[js.Any]])(
      SimpleForm()(
        ReferenceInput(_.source := "userId", _.reference := "users")(
          SelectInput(_.optionText := "name")
        ),
        TextInput(_.source := "title"),
        TextInput(_.source := "body")
      )
    )
  }

  def asElementType = component.toJsComponent.raw
}

object Main {
  private def App =
    Admin(
      _.dataProvider :=
        jsonServerProvider("https://jsonplaceholder.typicode.com")
    )(
      Resource(
        _.name := "posts",
        _.list := PostList.asElementType,
        _.edit := PostEdit.asElementType,
        _.create := PostCreate.asElementType
      ),
      Resource(_.name := "users", _.list := UserList.asElementType)
    )

  def main(args: Array[String]): Unit =
    App.render.renderIntoDOM(document.getElementById("root"))
}
