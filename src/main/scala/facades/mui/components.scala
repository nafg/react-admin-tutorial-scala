package tutorial
package facades.mui

import scala.scalajs.js

import japgolly.scalajs.react.vdom.VdomNode
import io.github.nafg.simplefacade.Implicits._
import io.github.nafg.simplefacade.{FacadeModule, PropTypes}


object Link extends FacadeModule.NodeChildren.Simple {
  override def raw = Material.Link
  override def mkProps = new Props
  class Props extends PropTypes.WithChildren[VdomNode] {
    val children = of[VdomNode]
    val href = of[String]
    val sx = of[js.Object]
  }
}

object LaunchIcon extends FacadeModule.Simple {
  override def raw = IconsMaterial.Launch
  override def mkProps = new Props
  class Props extends PropTypes {
    val sx = of[js.Object]
  }
}
