package tutorial
package facades.mui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport


@js.native
@JSImport("@mui/material", JSImport.Namespace)
object Material extends js.Any {
  val Link: js.Object = js.native
}

@js.native
@JSImport("@mui/icons-material", JSImport.Namespace)
object IconsMaterial extends js.Any {
  val Launch: js.Object = js.native
}
