package tutorial
package facades

import scala.annotation.unused
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

package object reactadmin {
  @js.native
  @JSImport("ra-data-json-server", JSImport.Default)
  def jsonServerProvider(@unused url: String): DataProvider = js.native
}
